from sklearn.datasets import make_moons
from sklearn.model_selection import train_test_split, GridSearchCV, RandomizedSearchCV
from sklearn.tree import DecisionTreeClassifier
from sklearn.metrics import accuracy_score
from scipy.stats import randint

X, y = make_moons(n_samples=10000, noise=0.4)

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42, shuffle=True)

dt_clf = DecisionTreeClassifier(max_depth=2, random_state=42)

param_grid = {
    'max_leaf_nodes': [10, 20, 30, 40, 50, 60, 70, 80, 90, 100]
}

grid_search = GridSearchCV(dt_clf, param_grid, cv=10, scoring='accuracy')
grid_search.fit(X_train, y_train)

best_params_grid = grid_search.best_params_
best_score_grid = grid_search.best_score_

print(f"Best parameters (GridSearchCV): {best_params_grid}")
print(f"Best cross-validation accuracy (GridSearchCV): {best_score_grid}")

param_dist = {
    'max_leaf_nodes': randint(10, 100)
}

random_search = RandomizedSearchCV(dt_clf, param_dist, n_iter=50, cv=10, scoring='accuracy', random_state=42)
random_search.fit(X_train, y_train)

best_params_random = random_search.best_params_
best_score_random = random_search.best_score_

print(f"Best parameters (RandomizedSearchCV): {best_params_random}")
print(f"Best cross-validation accuracy (RandomizedSearchCV): {best_score_random}")

best_dt_grid = grid_search.best_estimator_
y_pred_test_grid = best_dt_grid.predict(X_test)
test_accuracy_grid = accuracy_score(y_test, y_pred_test_grid)

print(f"Test accuracy (GridSearchCV best model): {test_accuracy_grid}")

best_dt_random = random_search.best_estimator_
y_pred_test_random = best_dt_random.predict(X_test)
test_accuracy_random = accuracy_score(y_test, y_pred_test_random)

print(f"Test accuracy (RandomizedSearchCV best model): {test_accuracy_random}")
